Thanks for checking this sample project out.
This is intended as a companion to my talk on Kotlin Coroutines: 
https://stldevfest.com/schedule/2019-01-26?sessionId=120
 
The code in this project is intended for educational purposes only.
Because we're hitting a real API, be sure you understand & are comfortable with the terms of the Google Books API before 
you compile and use this code: https://developers.google.com/books/terms

If you have any questions or concerns, please feel free to ping me on Twitter or on the public repository for this 
project.

You may not see it in the commit history, but I'm very grateful to the following folks that have helped to make this 
project and the accompanying talk possible:

* [name] – [Twitter handle if applicable] 
* Renee Vandervelde – @RenVandervelde
* Dan Lew – @danlew42
* Aaron Weaver 
* Brendon Justin – @brendonjustin_
* Nate Brunette – @natebrunette

Thanks to them for their patience and generosity with their time.