package com.poorfountain.introtocoroutines.model

/**
 * Represents the response to querying for a list of book volumes.
 * E.G. – https://www.googleapis.com/books/v1/volumes?q=quilting
 */
data class BookVolumesResponse(
    val kind: String,
    val totalItems: Long,
    val items: List<BookVolume>
)

data class BookVolume(
    val kind: String,
    val id: String,
    val volumeInfo: VolumeInfo
)

data class VolumeInfo(
    val title: String?,
    val subtitle: String?,
    val publisher: String?,
    val description: String?
)

enum class Kind(val value: String) {
    BooksVolume("books#volume")
}