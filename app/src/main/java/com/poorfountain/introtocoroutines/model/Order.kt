package com.poorfountain.introtocoroutines.model

/**
 * A placeholder representing an order. In a real project, this model would have values like "id", "lineItems", etc.
 */
data class Order(val id: String)
