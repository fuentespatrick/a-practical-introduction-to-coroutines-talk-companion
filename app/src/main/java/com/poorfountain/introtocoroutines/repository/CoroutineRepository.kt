package com.poorfountain.introtocoroutines.repository


import com.poorfountain.introtocoroutines.LoggingClass
import com.poorfountain.introtocoroutines.api.Global
import com.poorfountain.introtocoroutines.model.BookVolume
import com.poorfountain.introtocoroutines.model.Order
import com.poorfountain.introtocoroutines.model.User
import kotlinx.coroutines.delay

/**
 * An imagined repository intended to demonstrate how you can use coroutines to avoid callback hell
 */
class CoroutineRepository: LoggingClass {
    private var googleBooksApi = Global.googleBooksApi
    override var log: ((message: String) -> Unit)? = null

    suspend fun getBooks(searchTerm: String): List<BookVolume> {
        val response = googleBooksApi.getVolumesFor(searchTerm).await()
        return response.items
    }

    suspend fun getTotalOrderHistory(userId: String): List<Order> {
        // Some long running operation that gets a list of orders
        val orderList = mutableListOf<Order>()
        delay(1000)
        for (i in 1..100) {
            orderList.add(Order(i.toString()))
        }
        return orderList
    }

    suspend fun refreshUserData() {
        // Pretend this is some long running refresh operation
        delay(1500)
    }

    suspend fun getUser(): User {
        // Pretend this is some long running operation
        delay(1500)
        return User()
    }

    suspend fun slowOne() {
        log?.invoke("Started slowOne()")
        delay(1000)
        log?.invoke("Finished slowOne()")
    }

    suspend fun slowTwo() {
        log?.invoke("Started slowTwo()")
        delay(2000)
        log?.invoke("Finished slowTwo()")
    }

    suspend fun slowThree() {
        log?.invoke("Started slowThree()")
        delay(3000)
        log?.invoke("Finished slowThree()")
    }

}