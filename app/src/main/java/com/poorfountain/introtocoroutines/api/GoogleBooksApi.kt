package com.poorfountain.introtocoroutines.api

import com.poorfountain.introtocoroutines.model.BookVolumesResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface for accessing the Google Books API: https://developers.google.com/books/docs/v1/getting_started
 */
interface GoogleBooksApi {


    @GET("/books/v1/volumes?")
    fun getVolumesFor(
        @Query("q") searchTerm: String
    ): Deferred<BookVolumesResponse>


}