package com.poorfountain.introtocoroutines


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.poorfountain.introtocoroutines.repository.CoroutineRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), CoroutineScope {

    //These are necessary for implementing [CoroutineScope]
    private val activityJob = Job()
    override val coroutineContext: CoroutineContext = activityJob + Dispatchers.IO

    //These are demo variables for the examples
    private var userJob = Job()
    private val coroutineRepository = CoroutineRepository()
    private val arAssetGenerator = ArAssetGenerator(coroutineContext)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //set our click listeners
        clearLogsButton.setOnClickListener { clearLogs() }
        getBooksButton.setOnClickListener { getBooks() }
        runHeavyParallelWorkButton.setOnClickListener { arAssetGenerator.runHeavyParallelWork() }
        getPretendUserButton.setOnClickListener { getUser() }
        waitForThreeParallelCallsButton.setOnClickListener { waitForThreeSlowCallsParallel() }
        waitForThreeSequentialCallsButton.setOnClickListener { waitForThreeSlowCallsSequentially() }

        //Let the example classes log output to the screen for this sample app
        arAssetGenerator.connectLogger { message -> logMessage(message) }
        coroutineRepository.connectLogger { message -> logMessage(message) }

        //This shows how we could kick off a task to refresh our user data when this screen is first created
        this.userJob = launch {
            coroutineRepository.refreshUserData()
        }
    }

    override fun onStop() {
        super.onStop()
        //This is an example of how you would cancel a job that you don't want running anymore after the activity stops
        //being displayed
        this.userJob.cancel()
    }

    override fun onDestroy() {
        super.onDestroy()
        //This shows how we can cancel the scope of this activity when the activity is destroyed.
        //This will cancel any coroutines we launch from the scope of this activity, and it will cancel their children,
        //and the children of their children, etc.
        activityJob.cancel()
    }

    /**
     * This method shows how we can kick off a coroutine that returns a value
     */
    private fun getUser() {
        launch(Dispatchers.Main) {
            logMessage("Starting to retrieve an imaginary user")
            val deferredUser = async {
                coroutineRepository.getUser()
            }
            //Calling [Deferred#await] suspends the code running in this [CoroutineScope#launch] block until we get the value
            val user = deferredUser.await()
            //Do whatever you'd like with the user now
            logMessage("Finished retrieving an imaginary user")
        }
    }

    /**
     * This method shows how we can retrieve books from an API endpoint via our repository
     */
    private fun getBooks() {
        var searchTerm = bookSearchField.text.toString()
        if (searchTerm.isEmpty()) {
            searchTerm = "kotlin"
        }
        //See the note in [ArAssetGenerator] regarding why we use [Dispatchers.Main]
        launch(Dispatchers.Main) {
            logMessage("Starting to get books")
            val asyncBookJob = async {
                coroutineRepository.getBooks(searchTerm)
            }
            //Calling [Deferred#await] suspends the code running in this [CoroutineScope#launch] block until we get the value
            val books = asyncBookJob.await()
            //Do whatever you'd like with `books` here. For example, we can log them
            books.forEach { logMessage("Retrieved: ${it.volumeInfo.title} - ${it.volumeInfo.subtitle}") }
        }
    }

    /**
     * This method shows how we would wait for three calls to run in sequence (on at a time) before showing our last
     * call, and it'll wait until all three are done before it calls [callAfterThreeJobsAreDone]
     */
    private fun waitForThreeSlowCallsSequentially() {
        //See the note in [ArAssetGenerator] regarding why we use [Dispatchers.Main]
        launch(Dispatchers.Main) {
            logMessage("starting three sequential calls")
            val jobOne = launch {
                coroutineRepository.slowOne()
            }
            jobOne.join() //The call to [Job#join] suspends the code in this [CoroutineScope#launch] block until the job is done
            val jobTwo = launch {
                coroutineRepository.slowTwo()
            }
            jobTwo.join()
            val jobThree = launch {
                coroutineRepository.slowThree()
            }
            jobThree.join()
            callAfterThreeJobsAreDone()
        }
    }

    /**
     * This method is exactly like the one above, but it will execute the three suspending functions at the same time
     * (in parallel), and it'll wait until all three are done before it calls [callAfterThreeJobsAreDone]
     */
    private fun waitForThreeSlowCallsParallel() {
        //See the note in [ArAssetGenerator] regarding why we use [Dispatchers.Main]
        launch(Dispatchers.Main) {
            logMessage("starting three parallel calls")
            val jobOne = launch {
                coroutineRepository.slowOne()
            }
            val jobTwo = launch {
                coroutineRepository.slowTwo()
            }
            val jobThree = launch {
                coroutineRepository.slowThree()
            }
            jobOne.join()
            jobTwo.join()
            jobThree.join()
            callAfterThreeJobsAreDone()
        }
    }

    private fun callAfterThreeJobsAreDone() {
        logMessage("We finished all three jobs! Yay! \uD83C\uDF89")
    }

    private val TAG = this.javaClass.simpleName
    private var isFirstLog = true //used for clearing the text out of the window
    /**
     * This method is only for logging messages to logcat and the screen of the sample app at the same time
     */
    private fun logMessage(message: String) {
        //If this is the first time we have something to log, clear the placeholder text
        if (isFirstLog) {
            clearLogs()
            isFirstLog = false
        }
        //Log to the screen of the app
        logField.append("${Date()} – $message\n\n")
        //Log to Logcat
        Log.d(TAG, message)
    }

    /**
     * This method is just for clearing the on-screen logs in this sample app
     */
    private fun clearLogs() {
        logField.text = ""
    }
}

interface LoggingClass {
    var log: ((message: String) -> Unit)? //This function is only for logging to the screen

    /**
     * This method is only for logging to the screen in this sample app
     */
    fun connectLogger(loggingFunction: (message: String) -> Unit) {
        this.log = loggingFunction
    }
}