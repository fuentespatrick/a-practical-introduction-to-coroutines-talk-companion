I welcome contributions (and corrections, of course), but I advise that you talk to me first to ensure that your valuable time is well-spent.
For example, I may not want to merge some excellent pull requests that are more idiomatic if they're less clear.   
Let's connect to make sure we both feel great about changes.