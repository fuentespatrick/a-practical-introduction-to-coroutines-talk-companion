package com.poorfountain.introtocoroutines.model

/**
 * An imagined class representing a user. In a real project, this model would have values like "id", "screenName", etc.
 */
class User