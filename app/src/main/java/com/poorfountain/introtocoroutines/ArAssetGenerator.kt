package com.poorfountain.introtocoroutines

import com.poorfountain.introtocoroutines.model.Order
import com.poorfountain.introtocoroutines.repository.CoroutineRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

/**
 * An imagined class that generates 3D model assets for something, something Augmented Reality
 * Note that this is probably not how you want to handle [CoroutineScope] for this class. It's this way for simplicity
 */
class ArAssetGenerator(override val coroutineContext: CoroutineContext) : CoroutineScope, LoggingClass {

    private val coroutineRepository = CoroutineRepository()
    private val userId = "Pretend this is populated by something reasonable"
    private val arAssets = mutableListOf<ArAsset>()
    override var log: ((message: String) -> Unit)? = null

    /**
     * This method shows an example of running intense work in parallel
     */
    fun runHeavyParallelWork() {
        //We use [Dispatchers.Main] because the [log] function needs to affect the Main thread.
        //If you removed this and used the default dispatcher, the app would crash, and you'd see the following error:
        //"Only the original thread that created a view hierarchy can touch its views."
        launch(Dispatchers.Main) {
            //We get a big list of order objects. In this example, we need to make AR assets for each order
            val bigListOfOrders = coroutineRepository.getTotalOrderHistory(userId)
            log?.invoke("starting to run our heavy load of work in parallel")
            //For each order, kick off a coroutine to make an AR asset
            arAssets += bigListOfOrders.map { async { makeArAssetFromOrder(it) } }
                .awaitAll() //Wait until each [Deferred] object returns an AR asset before we proceed with the rest of
            // this [CoroutineScope#launch] block
            log?.invoke("ending work with ${arAssets.size} AR assets generated")
        }
    }

    /**
     * This method pretends to make an AR asset that takes a full second to create
     */
    private suspend fun makeArAssetFromOrder(order: Order): ArAsset {
        delay(1000)
        log?.invoke("finished making AR asset for order: ${order.id} on thread: ${Thread.currentThread().name}.")
        log?.invoke("My Job is ${coroutineContext[Job]}")
        return ArAsset()
    }
}

class ArAsset {
    //Pretend this is a 3D art asset for AR
}
